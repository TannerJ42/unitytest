﻿using UnityEngine;
using System.Collections;

public class GGExplosion : MonoBehaviour 
{
	int force = 500;
	
	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{

	}
	
	void Update()
	{
		if (Input.GetKeyDown (KeyCode.LeftControl)) 
		{
			rigidbody2D.AddForce(new Vector2(Random.Range (-force, force), Random.Range (-force, force)));
		}
	}
}
