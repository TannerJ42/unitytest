﻿using UnityEngine;
using System.Collections;

public class movingPlatform : MonoBehaviour 
{
	float timer;

	float moveDistance = 0.1f;

	State state;

	enum State
	{
		stopped,
		moving
	}

	// Use this for initialization
	void Start () 
	{

	
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		switch (state)
		{
		case (State.stopped):
			if (timer-- <= 0)
			{
				moveDistance *= -1;
				state = State.moving;
				timer = 60;
			}
			break;
		case (State.moving):
			transform.position = new Vector2(transform.position.x + moveDistance, transform.position.y);
			if (timer-- <= 0)
			{
				timer = 60;
				state = State.stopped;
			}
			break;
		}
	
	}
}
