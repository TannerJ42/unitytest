﻿using UnityEngine;
using System.Collections;
using InControl;

public class controllerText : MonoBehaviour 
{		
	public string currentText = "Test";
	
	void Awake ()
	{
		guiText.text = currentText;
	}
	
	void Update ()
	{
		currentText = "";

		for (int i = 0; i <= InputManager.Devices.Count - 1; i++)
		{
			InputDevice device = InputManager.Devices[i];

			currentText = currentText + InputManager.Devices[i].Name;

			foreach (InputControl g in InputManager.Devices[i].Controls)
				if (g != null && g.IsPressed)
					currentText += " is pressing " + g.ToString ();

			if (InputManager.Devices[i].Action1.IsPressed)
			{
				currentText = "A Pressed.";
			}
			if (InputManager.Devices[i].Action2.IsPressed)
			{
				currentText = "B Pressed.";
			}
			if (InputManager.Devices[i].Action3.IsPressed)
			{
				currentText = "X Pressed.";
			}
			if (InputManager.Devices[i].Action4.IsPressed)
			{
				currentText = "Y Pressed.";
			}

			if (device.RightBumper.IsPressed)
			{
				currentText = "Right bumper pressed.";
			}

			if (device.LeftBumper.IsPressed)
			{
				currentText = "Left bumper pressed.";
			}
			if (device.LeftTrigger.IsPressed)
			{
				currentText = "Left trigger pressed.";
			}
			if (device.RightTrigger.IsPressed)
			{
				currentText = "Right trigger pressed.";
			}
			if (device.Direction != Vector2.zero)
			{
				currentText = device.Direction.ToString ();
			}
			if (device.LeftStickVector != Vector2.zero)
			{
				currentText = device.LeftStickVector.ToString ();
			}
			if (device.RightStickVector != Vector2.zero)
			{
				currentText = device.RightStickVector.ToString ();
			}
			if (device.RightStickButton.IsPressed)
			{
				currentText = "Pressed right stick.";
			}
			if (device.LeftStickButton.IsPressed)
			{
				currentText = "Pressed left stick.";
			}

			if (device.GetControl(InputControlType.Back


			currentText += "\n";
		}
		// Set the score text.
		guiText.text = currentText;

	}
}