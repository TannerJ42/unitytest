﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


public class BarrierCheck : MonoBehaviour
{
	
	public Transform rootBG;
	private List<Transform> bgList;
	
	void Awake()
	{
		bgList = new List<Transform>();

		//get all BGs from the parent BG and put those on the list
		for (var i = 0; i < rootBG.childCount; i++)
		{
			bgList.Add(rootBG.GetChild(i));
		}

		for (int i = 0; i <= bgList.Count - 1; i ++) 
		{
			bgList[i].position = new Vector2(i * renderer.bounds.size.x, bgList[i].transform.position.y);
		}
		
		//ordem them all (left to right)
		bgList = bgList.OrderBy(t => t.position.x).ToList();
	}
	
	// Use this for initialization
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update()
	{
		
	}
	
	void OnTriggerExit2D(Collider2D c)
	{

		//when the first BG on the list exit the barrier trigger, this snippet is called
		Transform firstBG = bgList[0];
		Transform lastBG = bgList.Last();

		firstBG.position = new Vector2(lastBG.transform.position.x + firstBG.renderer.bounds.size.x, 0);

		//firstBG.Translate (
		//	new Vector2 (lastBG.transform.position.x + firstBG.renderer.bounds.size.x, firstBG.transform.position.y));
		
		bgList.Remove(firstBG);
		bgList.Add(firstBG);

	}
}