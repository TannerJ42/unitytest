﻿using UnityEngine;
using System.Collections;

public class CollisionExplosion : MonoBehaviour {

	public int multiplier = 20;
	public int timer;
	public Vector2 direction;

	// Use this for initialization
	void Start () 
	{
		timer = 2;
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (timer-- <= 0)
			Destroy (gameObject);
	}

	void OnTriggerEnter2D(Collider2D c)
	{
		Debug.Log ("Running OnTriggerEnter2D");

		//Vector2 force = c.transform.position - transform.position;
		//force.y = Mathf.Abs (force.y);

		direction *= multiplier;
		//c.rigidbody2D.velocity = new Vector2 ();
		c.rigidbody2D.AddForce (direction);



//		Debug.Log (transform.position);
//		
//		//when the first BG on the list exit the barrier trigger, this snippet is called
//		Transform firstBG = bgList[0];
//		Transform lastBG = bgList.Last();
//		
//		firstBG.position = new Vector2(lastBG.transform.position.x + firstBG.renderer.bounds.size.x, 0);
//		
//		//firstBG.Translate (
//		//	new Vector2 (lastBG.transform.position.x + firstBG.renderer.bounds.size.x, firstBG.transform.position.y));
//		
//		bgList.Remove(firstBG);
//		bgList.Add(firstBG);
//		
//		Debug.Log (transform.position);
	}

	void AddForce(Vector2 direction)
	{
		this.direction = direction;
	}
}
