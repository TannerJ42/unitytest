using UnityEngine;
using System.Collections;

using InControl;

public class GGControllerScript : MonoBehaviour 
{

	public float maxSpeed;
	private bool facingRight = true;

	public bool stunned;

	Animator anim;

	bool grounded = false;
	public Transform groundCheck;
	public float groundRadius;// = 0.2f;
	public LayerMask whatisground;
	public float jumpforce;// = 700f;
	bool doubleJump = false;

	//bool stunned = false;
	public int stunnedTime = 45;
	int stunnedTimer = 0;

	InputDevice device;
	InputControl jumpButton;
	InputControl attackButton;

	// Use this for initialization
	void Start ()
	{
		maxSpeed = 2.5f;
		anim = GetComponent<Animator> ();
		groundRadius = 0.05f;
		jumpforce = 200f;
	}

	// Update is called once per frame
	void FixedUpdate () 
	{
		if (device == null)
		{
			foreach (InputDevice d in InputManager.Devices)
			{
				if (device == null && d.Name == "Keyboard")
				{
					device = d;
					jumpButton = device.GetControl( InputControlType.Action1 );
					attackButton = device.GetControl (InputControlType.Action3);
				}
				Debug.Log ("Attached keyboard.");
			}
		}

		grounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatisground);
		anim.SetBool("Ground", grounded);
		anim.SetFloat("vspeed", rigidbody2D.velocity.y);

		if (grounded)
			doubleJump = false;
		

		float move = device.LeftStickX;

		anim.SetFloat ("speed", Mathf.Abs (move));



		if (!stunned) 
		{
			rigidbody2D.velocity = new Vector2 ((move * maxSpeed), rigidbody2D.velocity.y);

			if (move > 0 && !facingRight) 
			{
				Flip ();
			} 
			else if (move < 0 && facingRight) 
			{
				Flip ();
			}
		}
		else 
		{
			if (stunnedTimer-- <= 0)
			{
				UnStun();

			}
		}
	}

	void Update()
	{
		if (jumpButton.WasPressed)
		{
			if ((grounded || !doubleJump) && !stunned) 
			{
				rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, 0);
				rigidbody2D.AddForce(new Vector2(0, jumpforce));

				if (!doubleJump && !grounded)
				{
					doubleJump = true;
				}
			}
		}

		if (attackButton.WasPressed)
		{
			if (!stunned)
			{
				Vector2 location = transform.position;
				Vector2 direction = new Vector2(1, 0.5f);
				
				if (facingRight)
				{
					location.x += 0.5f;
				}
				else
				{
					direction.x = Mathf.Abs (direction.x);
					location.x -= 0.5f;
				}
				
				Debug.Log ("Attacking!");
				GameObject explosion = (GameObject)Instantiate(Resources.Load("Explosion"), location, Quaternion.identity);
				explosion.SendMessage("AddForce", direction, SendMessageOptions.RequireReceiver);
			}
		}
	}

	void Flip()
	{
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}



	public void Stun()
	{
		stunnedTimer = stunnedTime;
		stunned = true;
		//this.rigidbody2D.isKinematic = false;
	}

	public void UnStun()
	{
		//this.rigidbody2D.isKinematic = true;
		stunned = false;
	}
}
